% in this simulation we considered all the drag coefficients are constant
% which means they won't change with different AOA (angle of attack): alpha
% and according to the fluid dynamique we difined the drag coefficient
% of our paddles (streamlined body) cd_p equals to 0 
% as for the robot (half-sphere body) cd_robot = 0.42
% note the drag equation : Fd = 1/2 * rho * v * v * cd * A
% rudder == symetric airfoil
% for simplification :
% lift coefficient is defined proportionally to alpha
function Fr= getFr(alpha, v)
global robotDim const


% cd_robot = 0.42;
cl = abs(alpha);
A = robotDim.paddleSurf * sin(alpha);

% Fd = 1/2 * rho * v * v * cd_robot * robotDim.width*robotDim.height; 
Fr = 1/2 * const.rho * v * v * cl * A; 


